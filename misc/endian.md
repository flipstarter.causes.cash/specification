# Endian

[Endianness](https://en.wikipedia.org/wiki/Endianness) refers to the implies order of bytes for a given value.

Values are generally either [big-endian](/misc/endian/big), with the most significant bytes come first, or [little-endian](/misc/endian/little), with the least significant values first.  Nexa used a mix of little- and big-endian values, with little-endian byte order generally being used for integers.
