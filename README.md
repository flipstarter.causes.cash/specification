# Nexa Specification Repository

This repository contains markdown documentation that specifies the Nexa cryptocurrency protocol.

However, this repository should not be directly used for normal browsing because this document uses some markdown extensions.
Instead, read this documentation at one of the following places:

- [spec.nexa.org](https://spec.nexa.org)
- ...

