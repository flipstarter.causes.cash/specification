# Block Version History
| Beginning Height | Proposed In | Released | Summary |
|--|--|---|--|
|0||June 2022|The format used in the genesis block.|


**Note that Nexa blocks do not have a version field.  Nexa block versions are determined by height**
