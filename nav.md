
# Basics  

## [Overview](/)  
## [Blockchain Basics](/blockchain/blockchain)  
## [Chain work](/blockchain/chainwork-proof)  
## [Protocol Hashing Algorithms](/blockchain/hash)  
## [Transaction Pool](/blockchain/memory-pool)  

# The Blockchain  

## Addresses  
### [Address Types](/blockchain/addresses)  
### [Cashaddr Encoding](/blockchain/encoding/cashaddr)  

## Blocks  
### [The Block](/blockchain/block/block)  
### [Block Header](/blockchain/block/block-header)  
### [Merkle Tree](/blockchain/block/merkle-tree)  
### [Transaction Ordering](/blockchain/block/transaction-ordering)  

## Cryptography  
### [Bitcoin Keys (Public/Private)](/blockchain/cryptography/keys)  
### [Multisignature (M-of-N multisig)](/blockchain/cryptography/multisignature)  
### [Signatures (ECDSA/Schnorr)](/blockchain/cryptography/signatures)  

## Encoding  
### [Baase58](/blockchain/encoding/base58check)  
### [Cashaddr](/blockchain/encoding/cashaddr)  

## Proof of Work (PoW)
### [Difficulty Adjustment Algorithm](/blockchain/proof-of-work/difficulty-adjustment-algorithm)    
### [Mining](/blockchain/proof-of-work/mining)  
### [Proof of Work](/blockchain/proof-of-work/proof-of-work)  

## Script (Bitcoin transaction language)  
### [Operation Codes (opcodes)](/blockchain/script#operation-codes-opcodes)  
### [Script](/blockchain/script)  

## Tokens  
### [Native Tokenization](/blockchain/tokens/grouptokens)  
### [Token Description Document](/blockchain/tokens/tokenDescription)  

## Transactions  
### [Transactions Overview](/nexa/transactionOverview)  
### [Locking Script](/blockchain/transaction/locking-script)  
### [Transaction Format](/blockchain/transaction)  
### [Transaction Signing](/blockchain/transaction/transaction-signing)  
### [Unlocking Script](/blockchain/transaction/unlocking-script)  

## Transaction validation  
### [Block-Level Validation Rules](/blockchain/transaction-validation/block-level-validation-rules)  
### [Network-Level Validation Rules](/blockchain/transaction-validation/network-level-validation-rules)  
### [Transaction Validation](/blockchain/transaction-validation/transaction-validation)  

# Formats

## [Network Address](/formats/network-address)
## [Variable Length Integer](/formats/variable-length-integer)
## [Variable Length String](/formats/variable-length-string)

# History

## [Block Version](/history/block-version)
## [Protocol Version](/history/protocol-version)
## [Transaction Version](/history/transaction-version)

# Mining  

## [Echelon Protocol](/mining/echelon-protocol)  
## [Mining Pools](/mining/mining-pools)  

# Network protocol

## [Basic Network Parameters](/network/parameters)  
## [Handshake](/network/node-handshake)  
## [Network Messages](/network/messages)  

# Nexa

## [Address](/nexa/address)
## [Bignum](/nexa/bignum)
## [Bignum Modulo Divisor](/nexa/bignum_modulo_divisor)
## [Blinded POW](/nexa/blindedPOW)
## [CAPD](/nexa/capd)
## [Challenge Transaction](/nexa/challengeTransaction)
## [Delegated Payment Protocol](/nexa/dpp)
## [Nexa POW](/nexa/NexaPOW)
## [Nexa ID](/nexa/nexaid)
## [OP_BIN2BIGNUM](/nexa/op_bin2bignum)
## [OP_EXEC](/nexa/op_exec)
## [OP_NUM2BIN](/nexa/op_num2bin)
## [OP_PLACE](/nexa/op_place)
## [OP_PUSH_TX_STATE](/nexa/op_push_tx_state)
## [Opcode Syntax](/nexa/opcodesyntax)
## [Outpoint](/nexa/outpoint)
## [Script Templates](/nexa/scriptTemplates)
## [Sighash Type](/nexa/sighashtype)
## [Script Instructions](/nexa/scriptInstructions)
## [Transaction](/nexa/transaction)
## [Transaction Identifier](/nexa/transactionIdentifier)
## [Transaction Overview](/nexa/transactionOverview)

# Protocol Changes  

## [Addresses](/nexa/address)  
## [CAPD Message Pool (Counterparty and Protocol Discovery)](/nexa/capd)  
## [DPP (Delegated Payment Protocol)](/nexa/dpp)  
## [Nexa Identity](/nexa/nexid)  
## [Challenge Transactions](/nexa/challengeTransaction)  

# Simple Payment Verification (SPV)  

## [Bloom Filters](/spv/bloom-filter)  
## [SPV](/spv/spv)  

# Miscellaneous  

## [Endian](/misc/endian)  
