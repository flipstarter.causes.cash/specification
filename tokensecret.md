# Token Secrets
## Introduction
Let us suppose that Alice owns a token T.  This token tracks knowledge of a secret.

She would like to transfer that token and associated secret to Bob, using a public blockchain.

The system must contain the following properties.

1. The transfer transaction must transfer the token and reveal the secret decryption key to Bob atomically.
2. The transfer transaction cannot reveal any information about the key to a third party.  This allows the transfer transaction to exist on a public blockchain.
3. Bob or the blockchain must verify or enforce that Alice has communicated the same secret as was received.

## Use of Tokens Secrets to Decrypt Digital Resources

It is not possible to a-priori ensure that the secret actually decrypts an off-chain data file.  Similarly, the data file may contain something other than the promised data.  Therefore this protocol assumes that the initial token creator is honest.

However property 3 ensures that no dishonest party can substitute a different (invalid) secret during transfer.

Note that after transfer, Alice still knows the private key so may still decrypt the resource.  Additionally, Alice may publicly post the decryption key at any time, possibly destroying the value of the token.  And after she has transferred the token, there is no disincentive to keep the decryption key private.  Even if the above problems are solved, Alice may decrypt the digital resource while owning the token and post the plain text after transferring the token.

Therefore this system provides imperfect access to (for example) a media file.  Some or all of the described protocol may be placed in a trusted execution environment (TEE) to prevent Alice from gaining access to the decryption key and the unencrypted media, while allowing public, untrusted peer-to-peer access transfer.  However, the Android TEE is not customizable at this time.

Finally, depending on the media, Alice can often transcode it via an analog medium to defeat any DRM, for example by recording the media playing on a high quality display.  Therefore, DRM will always be at best a deterrent rather than a perfect solution.

## Protocol Overview

The communicated secret must be an EC private key, and the token commits to the secret's public key either in the token mint or as part of the group id.  Alice and Bob shall construct a transaction interactively using the "half-transaction" strategy.  In the set up Alice's input signature covers an input from Bob that requires 2 signatures (or checkdatasig) corresponding to the secret's pubkey and another key known only to Bob.

The transaction then incorporates an elliptic curve Diffie-Hellman key exchange, which chooses a shared secret.  This shared secret is then used to encrypt the secret and communicate it to Bob (this could happen outside of the transaction as well).  