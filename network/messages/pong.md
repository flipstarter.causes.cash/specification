<div class="cwikmeta" style="visibility:hidden;">
{
"title":"PONG",
"related":["/network/messages/ping.md"]
} </div>

# Response: Pong ("pong")

Connection keep-alive, "aliveness" and latency discovery.  This message is sent in response to a [`ping`](/network/messages/ping) message.

## Message Format

| Field | Length | Format | Description |
|--|--|--|--|
| nonce | 8 bytes | unsigned 64 bit integer<sup>[(LE)](/misc/endian/little)</sup>  | The value provided by the `ping` message.
