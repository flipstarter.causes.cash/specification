# Request: Mempool ("mempool")

Requests that the recipient notify the sender of transactions that are currently in its [mempool](/blockchain/memory-pool).
Recipients of a `mempool` message MAY respond with a set of transaction hashes currently in their mempool via an [`inv`](/network/messages/inv) message.

The mempool message was defined in [BIP-35](/forks/bip-0035).

## Message Format
This message has no contents.

