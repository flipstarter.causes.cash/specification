# Request: Send Headers (“sendheaders”)

Requests that new blocks are sent to the sender as a [`header`](/network/messages/headers) message instead of as a block hash [`inv`](/network/messages/inv) message.

## Message Format
This message has no contents.