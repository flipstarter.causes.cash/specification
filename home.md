<div class="cwikmeta">  
{  
"title": "NEXA",
"summary": "Nexa cryptocurrency consensus and protocol specifications"
} </div>


# Nexa Blockchain Protocol and Consensus Specifications
Last updated: 2023 May 9

## Overview  

Nexa is a decentralized cryptocurrency with a public distributed ledger.
Transactions are validated and transmitted over a peer-to-peer (P2P) overlay (over TCP-IP) network, and stored in the [blockchain](/blockchain/blockchain).
Nexa utilizes the Proof-of-Work timestamping and consensus scheme with a hash function that is a combination of elliptic curve multiplication and SHA-256, targeting a 2-minute block time.
It utilizes the secp256k1 parameters with the Schnorr algorithm for digital signatures.
The ticker symbol for Nexa is NEXA (or NEX if you must have 3 letters).

The Nexa consensus protocol is a modification of Bitcoin Cash, which is a modification of Bitcoin.  This documentation attempts to cover the full Nexa protocol, by leveraging previously written documentation for Bitcoin Cash.  Therefore you may occasionally see references to Bitcoin Cash that are no longer applicable.  Please notify us on [our issue tracker](https://gitlab.com/nexa/specification/-/issues) if you discover a problem.

[Style Guide](/style-guide) -- [Contributors](/contributors) -- [Target Audience](/target-audience) -- [Project History](/project-history)

[CWIK Markdown Cheat Sheet](/cwikMarkdownCheatSheet.md)  

## Design  

```mermaid
graph RL

  subgraph b0 [Block N-1]
  end

  subgraph b1 [Block N]
    hs1(Hash)
    ph1(Prev_Hash)
    ts1(Timestamp)
    mr1(Merkle Root)
  end

  subgraph b1_t [Merkle Tree]
    Hash0
    Hash1
    Hash2
    Hash3
    Hash01
    Hash23
    mr(Root)
  end

  subgraph b2[Block N+1]
  end

  classDef block fill: #AAA
  class b0,b1,b2 block

  b2 --> hs1
  ph1 --> b0

  mr --> mr1
  Hash01 --> mr
  Hash23 --> mr
  Tx0 --> Hash0
  Tx1 --> Hash1
  Tx2 --> Hash2
  Tx3 --> Hash3
  Hash0 --> Hash01
  Hash1 --> Hash01
  Hash2 --> Hash23
  Hash3 --> Hash23
```

Nexa operates on the blockchain which is replicated among the nodes in the Nexa overlay network.
[Transactions](/blockchain/transaction) are submitted to network nodes, which will [validate the transaction](/blockchain/transaction-validation/transaction-validation) against the transaction history in the blockchain.
Once transactions are considered valid, they will be grouped into blocks through [Merkle Trees](/blockchain/block/merkle-tree).
Through rigorous hash computation, blocks can be mined into the blockchain by the network nodes and are subsequently broadcast to the network.

The blockchain serves as the public ledger for the nexa cryptocurrency and participant-defined tokens.
It consists of a tree of blocks, where each block references its parent by cryptographic hash. Of this tree, one single chain of blocks probabilistically contains overwhelmingly more cumulative hashing work than any other chain.
This chain is called the "main chain" and its history, from genesis block to tip, defines the current state of the ledger, achieving consensus about what transactions are included in the blockchain and ledger.

Due to the characteristics of hash functions, the content of any block cannot be altered without changes to all its subsequent blocks.
The time and computation intensity of such change increases as the blockchain is extended with new blocks.

Therefore, the transaction history in the blockchain at an arbitrary point in the past can be considered probabilistically immutable even with public access, with the probability of immutability increasing the deeper in the chain the transaction is found.

The transactions on Nexa are pseudonymous.
The blockchain does not keep records of coin ownerships for users.
Instead, each transaction refers to the unspent outputs of previous transactions.
The outputs of the transaction is then locked through [locking scripts](/blockchain/transaction/locking-script) and whoever holds the correct [unlocking scripts](/blockchain/transaction/unlocking-script) can use the outputs in their future transactions.

The Nexa network is an ad-hoc decentralized network of volunteers, in which transactions are transmitted and validated.
Messages on the network are usually broadcast on a best-effort basis.



# Basics  

## [Overview](/)  
## [Blockchain Basics](/blockchain/blockchain)  
## [Chain work](/blockchain/chainwork-proof)  
## [Protocol Hashing Algorithms](/blockchain/hash)  
## [Transaction Pool](/blockchain/memory-pool)  

# The Blockchain  

## Addresses  
### [Address Types](/blockchain/addresses)  
### [Cashaddr Encoding](/blockchain/encoding/cashaddr)  

## Blocks  
### [The Block](/blockchain/block/block)  
### [Block Header](/blockchain/block/block-header)  
### [Merkle Tree](/blockchain/block/merkle-tree)  
### [Transaction Ordering](/blockchain/block/transaction-ordering)  

## Cryptography  
### [Bitcoin Keys (Public/Private)](/blockchain/cryptography/keys)  
### [Multisignature (M-of-N multisig)](/blockchain/cryptography/multisignature)  
### [Signatures (ECDSA/Schnorr)](/blockchain/cryptography/signatures)  

## Encoding  
### [Base58](/blockchain/encoding/base58check)  
### [Cashaddr](/blockchain/encoding/cashaddr)  

## Proof of Work (PoW)
### [Difficulty Adjustment Algorithm](/blockchain/proof-of-work/difficulty-adjustment-algorithm)    
### [Mining](/blockchain/proof-of-work/mining)  
### [Proof of Work](/blockchain/proof-of-work/proof-of-work)  

## Script (Bitcoin transaction language)  
### [Operation Codes (opcodes)](/blockchain/script#operation-codes-opcodes)  
### [Script](/blockchain/script)  

## Tokens  
### [Native Tokenization](/blockchain/tokens/grouptokens)  
### [Token Description Document](/blockchain/tokens/tokenDescription)  
### [NFT/SFT Specification](/nexa/nft)
### [NFT/SFT Categories](/nexa/NFTcategories)

## Transactions  
### [NEXA Transactions Overview](/nexa/transactionOverview)  
### [Template Script Output Format](/nexa/scriptTemplates)
### [Locking Script](/blockchain/transaction/locking-script)  
### [Transaction Format](/blockchain/transaction)  
### [Transaction Signing](/blockchain/transaction/transaction-signing)  
### [Unlocking Script](/blockchain/transaction/unlocking-script)  

## Transaction validation  
### [Block-Level Validation Rules](/blockchain/transaction-validation/block-level-validation-rules)  
### [Network-Level Validation Rules](/blockchain/transaction-validation/network-level-validation-rules)  
### [Transaction Validation](/blockchain/transaction-validation/transaction-validation)  

# Formats

## [Network Address](/formats/network-address)
## [Variable Length Integer](/formats/variable-length-integer)
## [Variable Length String](/formats/variable-length-string)

# History

## [Block Version](/history/block-version)
## [Protocol Version](/history/protocol-version)
## [Transaction Version](/history/transaction-version)

# Mining  

## [Echelon Protocol](/mining/echelon-protocol)  
## [Mining Pools](/mining/mining-pools)  

# Network protocol

## [Basic Network Parameters](/network/parameters)  
## [Handshake](/network/node-handshake)  
## [Network Messages](/network/messages)  

# Nexa

## [Address](/nexa/address)
## [Bignum](/nexa/bignum)
## [Bignum Modulo Divisor](/nexa/bignum_modulo_divisor)
## [Blinded POW](/nexa/blindedPOW)
## [CAPD](/nexa/capd)
## [Challenge Transaction](/nexa/challengeTransaction)
## [Delegated Payment Protocol](/nexa/dpp)
## [Nexa POW](/nexa/NexaPOW)
## [Nexa ID](/nexa/nexaid)
## [OP_BIN2BIGNUM](/nexa/op_bin2bignum)
## [OP_EXEC](/nexa/op_exec)
## [OP_NUM2BIN](/nexa/op_num2bin)
## [OP_PLACE](/nexa/op_place)
## [OP_PUSH_TX_STATE](/nexa/op_push_tx_state)
## [Opcode Syntax](/nexa/opcodesyntax)
## [Outpoint](/nexa/outpoint)
## [Script Templates](/nexa/scriptTemplates)
## [Sighash Type](/nexa/sighashtype)
## [Script Instructions](/nexa/scriptInstructions)
## [Transaction](/nexa/transaction)
## [Transaction Identifier](/nexa/transactionIdentifier)
## [Transaction Overview](/nexa/transactionOverview)

# Protocol Changes  

## [Addresses](/nexa/address)  
## [CAPD Message Pool (Counterparty and Protocol Discovery)](/nexa/capd)  
## [DPP (Delegated Payment Protocol)](/nexa/dpp)  
## [Nexa Identity](/nexa/nexid)  
## [Challenge Transactions](/nexa/challengeTransaction)  

# Simple Payment Verification (SPV)  

## [Bloom Filters](/spv/bloom-filter)  
## [SPV](/spv/spv)  

# Miscellaneous  

## [Endian](/misc/endian)  